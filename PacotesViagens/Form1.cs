﻿namespace PacotesViagens
{
    using System;
    using System.Windows.Forms;
    using Modelos;
    using System.Collections.Generic;
    using System.IO;

    public partial class Form1 : Form
    {
        private List<Pacotes> PacotesTuristicos;

        private FormEditarPacotes formularioEditarPacotes;
        private FormApagarPacotes formularioApagarPacotes;
        private FormVerPacotes formularioVerPacotes;
        private FormCriarPacotes formularioCriarPacotes;
        private Pacotes pacote = new Pacotes();

        private string fileName = null;

        public Form1()
        {
            InitializeComponent();
            ControlBox = false;

            Salvar();

            PacotesTuristicos = new List<Pacotes>();

            //PacotesTuristicos.Add(new Pacotes { IdPacotes = 1, DescricaoPacotes = "Viagem para 2 pessoas as Caraibas, 5 noites, tudo incluido", PrecoPacotes = 1000 });
            //PacotesTuristicos.Add(new Pacotes { IdPacotes = 2, DescricaoPacotes = "Viagem para 2 pessoas as Dubrovnik, 5 noites, pensão completa", PrecoPacotes = 2000 });
            //PacotesTuristicos.Add(new Pacotes { IdPacotes = 3, DescricaoPacotes = "Viagem romantica a 2, para Paris, 3 noites, tudo incluido", PrecoPacotes = 400 });
            //PacotesTuristicos.Add(new Pacotes { IdPacotes = 4, DescricaoPacotes = "Viagem para 2 pessoas as Miami, 5 noites, tudo incluido", PrecoPacotes = 3000 });

        }

        private void Salvar()
        {
            if (fileName == null)
            {
                gravarToolStripMenuItem.Enabled = false; //Opção Salvar
            }
            else
            {
                gravarToolStripMenuItem.Enabled = true; //Opção Salvar
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonRead_Click(object sender, EventArgs e)
        {
            if (PacotesTuristicos.Count == 0)
            {
                MessageBox.Show("Não existe nenhum pacote para ver");
            }
            else
            {
                formularioVerPacotes = new FormVerPacotes(PacotesTuristicos);
                formularioVerPacotes.Show();
            }

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hugo Pereira\nVersão (V1.0.1)\n09-10-2016");
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {

            if (PacotesTuristicos.Count == 0)
            {
                MessageBox.Show("Não existe nenhum pacote para editar");
            }
            else
            {
                formularioEditarPacotes = new FormEditarPacotes(PacotesTuristicos);
                formularioEditarPacotes.Show();
            }
        }

        private void ButtonCreate_Click(object sender, EventArgs e)
        {
            formularioCriarPacotes = new FormCriarPacotes(PacotesTuristicos);
            formularioCriarPacotes.Show();

        }

        private void BUttonDelete_Click(object sender, EventArgs e)
        {

            if (PacotesTuristicos.Count == 0)
            {
                MessageBox.Show("Não existe nenhum pacote para apagar");
            }
            else
            {
                formularioApagarPacotes = new FormApagarPacotes(PacotesTuristicos);
                formularioApagarPacotes.Show();
            }

        }

        private void gravarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalvarComo();
            Salvar();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader myStream;
            OpenFileDialog ficheiro = new OpenFileDialog();
            ficheiro.Filter = "Text file|*.txt";


            if (ficheiro.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = new StreamReader(ficheiro.OpenFile())) != null)
                    {
                        fileName = ficheiro.FileName;
                        using (myStream)
                        {
                            PacotesTuristicos.Clear();
                            string linha = "";

                            while ((linha = myStream.ReadLine()) != null)
                            {
                                string[] campos = new string[3];
                                campos = linha.Split(';');

                                var pacote = new Pacotes
                                {
                                    IdPacotes = Convert.ToInt32(campos[0]),
                                    DescricaoPacotes = campos[1],
                                    PrecoPacotes = Convert.ToDouble(campos[2])
                                };

                                PacotesTuristicos.Add(pacote);
                            }

                            myStream.Close();
                        }
                    }
                    Salvar();
                }
                catch (Exception erro)
                {
                    MessageBox.Show("Não foi possivel ler o ficheiro.\nErro: " + erro.Message);
                }
            }

        }

        private void SalvarComo()
        {

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Ficheiro de texto|*.txt";
            saveFileDialog1.Title = "Salvar como...";
            saveFileDialog1.ShowDialog();


            if (saveFileDialog1.FileName != "")
            {
                FileStream fs = (FileStream)saveFileDialog1.OpenFile(); // Cria o ficheiro

                StreamWriter sw = new StreamWriter(fs); // Abre a opção de escrita da linha

                fileName = saveFileDialog1.FileName;

                foreach (var pacotes in PacotesTuristicos)
                {
                    string linha = string.Format("{0};{1};{2}", pacotes.IdPacotes, pacotes.DescricaoPacotes, pacotes.PrecoPacotes);
                    sw.WriteLine(linha); // escreve no ficheiro cada linha
                }
                sw.Close();

                fs.Close();
            }
        }

        private void gravarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileName != null)
            {

                StreamWriter sw = new StreamWriter(fileName, false);

                try
                {
                    if (!File.Exists(fileName))
                    {
                        sw = File.CreateText(fileName);
                    }

                    foreach (var pacotes in PacotesTuristicos)
                    {
                        string linha = string.Format("{0};{1};{2}", pacotes.IdPacotes, pacotes.DescricaoPacotes, pacotes.PrecoPacotes);
                        sw.WriteLine(linha);
                    }

                    sw.Close();
                }
                catch (Exception erro)
                {
                    MessageBox.Show(erro.Message);
                }

                return;
            }
            SalvarComo();
            
        }
    }
}
