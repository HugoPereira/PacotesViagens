﻿namespace PacotesViagens
{
    partial class FormVerPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridViewVerPacotes = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewVerPacotes)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewVerPacotes
            // 
            this.DataGridViewVerPacotes.AllowUserToResizeRows = false;
            this.DataGridViewVerPacotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewVerPacotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.DataGridViewVerPacotes.Location = new System.Drawing.Point(12, 36);
            this.DataGridViewVerPacotes.Name = "DataGridViewVerPacotes";
            this.DataGridViewVerPacotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DataGridViewVerPacotes.Size = new System.Drawing.Size(444, 267);
            this.DataGridViewVerPacotes.TabIndex = 0;
            this.DataGridViewVerPacotes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewVerPacotes_CellContentClick);
            // 
            // Column1
            // 
            dataGridViewCellStyle3.NullValue = null;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.FillWeight = 37.40648F;
            this.Column1.HeaderText = "Id Pacote";
            this.Column1.Name = "Column1";
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 211.5782F;
            this.Column2.HeaderText = "Descrição";
            this.Column2.Name = "Column2";
            this.Column2.Width = 283;
            // 
            // Column3
            // 
            dataGridViewCellStyle4.NullValue = null;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.FillWeight = 51.01527F;
            this.Column3.HeaderText = "Preço";
            this.Column3.Name = "Column3";
            this.Column3.Width = 68;
            // 
            // ButtonClose
            // 
            this.ButtonClose.Image = global::PacotesViagens.Properties.Resources.icon_fechar;
            this.ButtonClose.Location = new System.Drawing.Point(477, 310);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(40, 40);
            this.ButtonClose.TabIndex = 1;
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // FormVerPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 351);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.DataGridViewVerPacotes);
            this.Name = "FormVerPacotes";
            this.Text = "Pacotes Existentes";
            this.Load += new System.EventHandler(this.FormVerPacotes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewVerPacotes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewVerPacotes;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}