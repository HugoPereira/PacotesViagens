﻿namespace PacotesViagens
{
    partial class FormCriarPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.TextBoxDescricaoPacote = new System.Windows.Forms.TextBox();
            this.TextBoxPrecoPacote = new System.Windows.Forms.TextBox();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ButtonCriarPacote = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Location = new System.Drawing.Point(94, 23);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.ReadOnly = true;
            this.TextBoxIdPacote.Size = new System.Drawing.Size(207, 20);
            this.TextBoxIdPacote.TabIndex = 0;
            // 
            // TextBoxDescricaoPacote
            // 
            this.TextBoxDescricaoPacote.Location = new System.Drawing.Point(94, 72);
            this.TextBoxDescricaoPacote.Multiline = true;
            this.TextBoxDescricaoPacote.Name = "TextBoxDescricaoPacote";
            this.TextBoxDescricaoPacote.Size = new System.Drawing.Size(207, 80);
            this.TextBoxDescricaoPacote.TabIndex = 1;
            // 
            // TextBoxPrecoPacote
            // 
            this.TextBoxPrecoPacote.Location = new System.Drawing.Point(94, 182);
            this.TextBoxPrecoPacote.Name = "TextBoxPrecoPacote";
            this.TextBoxPrecoPacote.Size = new System.Drawing.Size(207, 20);
            this.TextBoxPrecoPacote.TabIndex = 2;
            // 
            // ButtonClose
            // 
            this.ButtonClose.Image = global::PacotesViagens.Properties.Resources.icon_fechar;
            this.ButtonClose.Location = new System.Drawing.Point(355, 252);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(40, 40);
            this.ButtonClose.TabIndex = 3;
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Descrição";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Preço";
            // 
            // ButtonCriarPacote
            // 
            this.ButtonCriarPacote.Image = global::PacotesViagens.Properties.Resources.ic_ok;
            this.ButtonCriarPacote.Location = new System.Drawing.Point(355, 23);
            this.ButtonCriarPacote.Name = "ButtonCriarPacote";
            this.ButtonCriarPacote.Size = new System.Drawing.Size(40, 40);
            this.ButtonCriarPacote.TabIndex = 7;
            this.ButtonCriarPacote.UseVisualStyleBackColor = true;
            this.ButtonCriarPacote.Click += new System.EventHandler(this.ButtonCriarPacote_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Image = global::PacotesViagens.Properties.Resources.ic_cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(355, 94);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(40, 40);
            this.ButtonCancel.TabIndex = 8;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // FormCriarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 292);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonCriarPacote);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.TextBoxPrecoPacote);
            this.Controls.Add(this.TextBoxDescricaoPacote);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Name = "FormCriarPacotes";
            this.Text = "FormCriarPacotes";
            this.Load += new System.EventHandler(this.FormCriarPacotes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.TextBox TextBoxDescricaoPacote;
        private System.Windows.Forms.TextBox TextBoxPrecoPacote;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ButtonCriarPacote;
        private System.Windows.Forms.Button ButtonCancel;
    }
}