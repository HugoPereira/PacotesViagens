﻿

namespace PacotesViagens
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormCriarPacotes : Form
    {
        private List<Pacotes> PacotesTuristicos;
        private Pacotes geranum;

       

        public FormCriarPacotes(List<Pacotes> PacotesTuristicos)
        {
            
            geranum = new Pacotes();
            InitializeComponent();
            ControlBox = false;
            this.PacotesTuristicos = PacotesTuristicos;

            if (PacotesTuristicos.Count > 1)
            {
                geranum.SetNum(Convert.ToInt32(PacotesTuristicos[PacotesTuristicos.Count - 1].IdPacotes));
                TextBoxIdPacote.Text = geranum.GetNum().ToString();
            }
            else
            {
                TextBoxIdPacote.Text = geranum.GetNum().ToString();
            }

            
            

        }

       

        private void ButtonClose_Click(object sender, System.EventArgs e)
        {
            Close();
           
        }

        private bool verificaPreco(string preco)
        {
            double numero;

            if (!double.TryParse(preco, out numero))
            {
                return true;
            }
            return false;
        }

        private void ButtonCriarPacote_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricaoPacote.Text))
            {
                MessageBox.Show("Insira uma descrição do pacote");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxPrecoPacote.Text) || verificaPreco(TextBoxPrecoPacote.Text))
            {
                MessageBox.Show("Insira um preço para este pacote");
                return;
            }

            var pacote = new Pacotes
            {
                IdPacotes = Convert.ToInt32(TextBoxIdPacote.Text),
                DescricaoPacotes = TextBoxDescricaoPacote.Text,
                PrecoPacotes = Convert.ToDouble(TextBoxPrecoPacote.Text)
            };

            PacotesTuristicos.Add(pacote); //metemos o novo aluno na lista
            MessageBox.Show("Pacote criado com sucesso.");
            Refresh();
            TextBoxDescricaoPacote.Clear();
            TextBoxPrecoPacote.Clear();
            TextBoxIdPacote.Text = geranum.GetNum().ToString();
            Close();

        }
        private Pacotes num = new Pacotes();

       

        private void FormCriarPacotes_Load(object sender, EventArgs e)
        {

        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O pacote não foi criado!");
        }
    }
}
