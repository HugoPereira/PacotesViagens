﻿
namespace PacotesViagens
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;


    public partial class FormApagarPacotes : Form
    {
        private List<Pacotes> PacotesTuristicos;
        
        public FormApagarPacotes(List<Pacotes> PacotesTuristicos)
        {
            InitializeComponent();
            this.PacotesTuristicos = PacotesTuristicos;
            ControlBox = false;
            
            foreach (var pacotes in PacotesTuristicos)
            {
                ComboBoxPacotes.Items.Add(pacotes.IdPacotes + " - " + pacotes.DescricaoPacotes);
            }
            
        }

        private void FormApagarPacotes_Load(object sender, EventArgs e)
        {
           
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ComboBoxPacotes.SelectedIndex == -1)//combobox esta vazia, nao se escolheu nada
            {
                MessageBox.Show("Tem de escolher um pacote");
                return;
            }
            else
            {
                PacotesTuristicos.RemoveAt(ComboBoxPacotes.SelectedIndex);
                MessageBox.Show("Pacote apagado com sucesso!");
                Close();
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pacote não foi apagado!");
            Close();
        }
    }
}
