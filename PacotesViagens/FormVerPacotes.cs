﻿namespace PacotesViagens
{
    using System;
    using System.Windows.Forms;
    using Modelos;
    using System.Collections.Generic;

    public partial class FormVerPacotes : Form
    {
        private List<Pacotes> PacotesTuristicos = new List<Pacotes>();
       
        

        public FormVerPacotes(List<Pacotes> PacotesTuristicos)
        {
            
            InitializeComponent();
            ControlBox = false;
            this.PacotesTuristicos = PacotesTuristicos;

            for (int i = 0; i < PacotesTuristicos.Count; i++)
            {
                DataGridViewVerPacotes.Rows.Add(PacotesTuristicos[i].IdPacotes, PacotesTuristicos[i].DescricaoPacotes, PacotesTuristicos[i].PrecoPacotes);
            }
            

            DataGridViewVerPacotes.AllowUserToAddRows = false;
        }

        private void DataGridViewVerPacotes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormVerPacotes_Load(object sender, EventArgs e)
        {

        }
    }
}
