﻿namespace PacotesViagens.Modelos
{
    public class Pacotes
    {
        //public int IdPacotes { get; set; }

        public int IdPacotes { get ;  set ;  }

        public string DescricaoPacotes { get; set; }

        public double PrecoPacotes { get; set; }

        public override string ToString()
        {
            return string.Format("Id Pacote: {0} Descrição: {1} Preço: {2}", IdPacotes, DescricaoPacotes, PrecoPacotes);
        }
        private int num;

        public int SetNum(int value)
        {
            return num = value;
        }

        public int GetNum()
        {
            return num + 1;
        }
    }
}
