﻿namespace PacotesViagens
{
    partial class FormApagarPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxPacotes = new System.Windows.Forms.ComboBox();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboBoxPacotes
            // 
            this.ComboBoxPacotes.FormattingEnabled = true;
            this.ComboBoxPacotes.Location = new System.Drawing.Point(23, 39);
            this.ComboBoxPacotes.Name = "ComboBoxPacotes";
            this.ComboBoxPacotes.Size = new System.Drawing.Size(292, 21);
            this.ComboBoxPacotes.TabIndex = 0;
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Image = global::PacotesViagens.Properties.Resources.ic_ok;
            this.ButtonDelete.Location = new System.Drawing.Point(396, 20);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(40, 40);
            this.ButtonDelete.TabIndex = 1;
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonClose
            // 
            this.ButtonClose.Image = global::PacotesViagens.Properties.Resources.icon_fechar;
            this.ButtonClose.Location = new System.Drawing.Point(396, 243);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(40, 40);
            this.ButtonClose.TabIndex = 2;
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Image = global::PacotesViagens.Properties.Resources.ic_cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(396, 92);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(40, 40);
            this.ButtonCancel.TabIndex = 3;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // FormApagarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 283);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.ButtonDelete);
            this.Controls.Add(this.ComboBoxPacotes);
            this.Name = "FormApagarPacotes";
            this.Text = "Apagar Pacotes";
            this.Load += new System.EventHandler(this.FormApagarPacotes_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxPacotes;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Button ButtonCancel;
    }
}