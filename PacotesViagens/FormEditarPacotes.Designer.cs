﻿namespace PacotesViagens
{
    partial class FormEditarPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonPacotePosterior = new System.Windows.Forms.Button();
            this.ButtonPacoteAnterior = new System.Windows.Forms.Button();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.ButtonAlterar = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.TextBoxId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ButtonPacotePosterior
            // 
            this.ButtonPacotePosterior.Image = global::PacotesViagens.Properties.Resources.icon_arrow_rt;
            this.ButtonPacotePosterior.Location = new System.Drawing.Point(210, 176);
            this.ButtonPacotePosterior.Name = "ButtonPacotePosterior";
            this.ButtonPacotePosterior.Size = new System.Drawing.Size(50, 50);
            this.ButtonPacotePosterior.TabIndex = 1;
            this.ButtonPacotePosterior.UseVisualStyleBackColor = true;
            this.ButtonPacotePosterior.Click += new System.EventHandler(this.ButtonPacotePosterior_Click);
            // 
            // ButtonPacoteAnterior
            // 
            this.ButtonPacoteAnterior.Image = global::PacotesViagens.Properties.Resources.icon_arrow_lt;
            this.ButtonPacoteAnterior.Location = new System.Drawing.Point(36, 176);
            this.ButtonPacoteAnterior.Name = "ButtonPacoteAnterior";
            this.ButtonPacoteAnterior.Size = new System.Drawing.Size(50, 50);
            this.ButtonPacoteAnterior.TabIndex = 0;
            this.ButtonPacoteAnterior.UseVisualStyleBackColor = true;
            this.ButtonPacoteAnterior.Click += new System.EventHandler(this.ButtonPacoteAnterior_Click);
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(36, 52);
            this.TextBoxDescricao.Multiline = true;
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.ReadOnly = true;
            this.TextBoxDescricao.Size = new System.Drawing.Size(224, 61);
            this.TextBoxDescricao.TabIndex = 2;
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(36, 132);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.ReadOnly = true;
            this.TextBoxPreco.Size = new System.Drawing.Size(224, 20);
            this.TextBoxPreco.TabIndex = 3;
            // 
            // ButtonClose
            // 
            this.ButtonClose.Image = global::PacotesViagens.Properties.Resources.icon_fechar;
            this.ButtonClose.Location = new System.Drawing.Point(424, 191);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(40, 40);
            this.ButtonClose.TabIndex = 4;
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // ButtonAlterar
            // 
            this.ButtonAlterar.Image = global::PacotesViagens.Properties.Resources.icon_update;
            this.ButtonAlterar.Location = new System.Drawing.Point(424, 24);
            this.ButtonAlterar.Name = "ButtonAlterar";
            this.ButtonAlterar.Size = new System.Drawing.Size(40, 40);
            this.ButtonAlterar.TabIndex = 5;
            this.ButtonAlterar.UseVisualStyleBackColor = true;
            this.ButtonAlterar.Click += new System.EventHandler(this.ButtonAlterar_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Image = global::PacotesViagens.Properties.Resources.ic_cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(424, 85);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(40, 40);
            this.ButtonCancel.TabIndex = 6;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // TextBoxId
            // 
            this.TextBoxId.Location = new System.Drawing.Point(36, 13);
            this.TextBoxId.Name = "TextBoxId";
            this.TextBoxId.ReadOnly = true;
            this.TextBoxId.Size = new System.Drawing.Size(224, 20);
            this.TextBoxId.TabIndex = 7;
            // 
            // FormEditarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 232);
            this.Controls.Add(this.TextBoxId);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonAlterar);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.ButtonPacotePosterior);
            this.Controls.Add(this.ButtonPacoteAnterior);
            this.Name = "FormEditarPacotes";
            this.Text = "Editar Pacotes";
            this.Load += new System.EventHandler(this.FormEditarPacotes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonPacoteAnterior;
        private System.Windows.Forms.Button ButtonPacotePosterior;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Button ButtonAlterar;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.TextBox TextBoxId;
    }
}