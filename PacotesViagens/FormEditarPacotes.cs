﻿

namespace PacotesViagens
{
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;
    using System;
    using System.Drawing;

    public partial class FormEditarPacotes : Form
    {
        private List<Pacotes> PacotesTuristicos;
        private int num = 0;
        public FormEditarPacotes(List<Pacotes> PacotesTuristicos)
        {
            InitializeComponent();
            ControlBox = false;
            this.PacotesTuristicos = PacotesTuristicos;
            TextBoxId.Text = PacotesTuristicos[num].IdPacotes.ToString();
            TextBoxDescricao.Text = PacotesTuristicos[num].DescricaoPacotes;
            TextBoxPreco.Text = PacotesTuristicos[num].PrecoPacotes.ToString();
        }

        private void FormEditarPacotes_Load(object sender, System.EventArgs e)
        {

        }

        private void ButtonClose_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void ButtonPacoteAnterior_Click(object sender, System.EventArgs e)
        {
            if (num == 0)
            {
               
                
                TextBoxId.Text = PacotesTuristicos[0].IdPacotes.ToString();
                TextBoxDescricao.Text = Convert.ToString(PacotesTuristicos[0].DescricaoPacotes);
                TextBoxPreco.Text = Convert.ToString(PacotesTuristicos[0].PrecoPacotes);
                ButtonPacoteAnterior.Enabled = false;
                ButtonPacotePosterior.Enabled = true;

            }
            else
            {
                num -= 1;
                TextBoxId.Text = PacotesTuristicos[num].IdPacotes.ToString();
                TextBoxDescricao.Text = Convert.ToString(PacotesTuristicos[num].DescricaoPacotes);
                TextBoxPreco.Text = Convert.ToString(PacotesTuristicos[num].PrecoPacotes);
                ButtonPacoteAnterior.Enabled = true;
                ButtonPacotePosterior.Enabled = true;

            }
           

        }

        private void ButtonPacotePosterior_Click(object sender, EventArgs e)
        {
            if (num == PacotesTuristicos.Count-1)
            {
                
                TextBoxId.Text = PacotesTuristicos[PacotesTuristicos.Count - 1].IdPacotes.ToString();
                TextBoxDescricao.Text = Convert.ToString(PacotesTuristicos[PacotesTuristicos.Count-1].DescricaoPacotes);
                TextBoxPreco.Text = Convert.ToString(PacotesTuristicos[PacotesTuristicos.Count - 1].PrecoPacotes);
                ButtonPacotePosterior.Enabled = false;
                ButtonPacoteAnterior.Enabled = true;

            }
            else
            {
                num += 1;
                TextBoxId.Text = PacotesTuristicos[num].IdPacotes.ToString();
                TextBoxDescricao.Text = Convert.ToString(PacotesTuristicos[num].DescricaoPacotes);
                TextBoxPreco.Text = Convert.ToString(PacotesTuristicos[num].PrecoPacotes);
                ButtonPacoteAnterior.Enabled = true;
                ButtonPacotePosterior.Enabled = true;

            }
            
        }

        private bool estado = true;

        private void ButtonAlterar_Click(object sender, EventArgs e)
        {
            if (estado)
            {
                estado = false;
                ButtonAlterar.Image = Properties.Resources.icon_save;
                TextBoxDescricao.ReadOnly = false;
                TextBoxPreco.ReadOnly = false;
            }
            else
            {
               
                estado = true;
                ButtonAlterar.Image = Properties.Resources.icon_update;
                TextBoxDescricao.ReadOnly = true;
                TextBoxPreco.ReadOnly = true;
                PacotesTuristicos[num].DescricaoPacotes = TextBoxDescricao.Text;
                PacotesTuristicos[num].PrecoPacotes = Convert.ToDouble(TextBoxPreco.Text);
                MessageBox.Show("O pacote editado com sucesso!");
                Close();
            }
                
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O pacote não foi editado!");
            Close();
            
        }
    }
}
